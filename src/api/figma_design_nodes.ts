import { get } from './request';

export async function fetchDesignNode(ids: string[]) {
  const idsStr = ids.join(',');

  // Get the current version of nodes
  const nodes = await get(`https://api.figma.com/v1/files/Tf7yMr1BpRvwOkV9GWafBE/nodes?ids=${idsStr}`);
  return JSON.parse(nodes);
}
