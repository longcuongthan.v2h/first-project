// @ts-nocheck

const https = require('https');

export function get(url): Promise<any> {
  return new Promise((resolve) => {
    https.get(
      url,
      {
        headers: {
          'Content-Type': 'application/json',
          'x-figma-token': '143701-cb79b2cc-bdb5-49ae-9093-26992678c149',
        },
      },
      (response) => {
        let data = '';
        response.on('data', (_data) => (data += _data));
        response.on('end', () => resolve(data));
      },
    );
  });
}
