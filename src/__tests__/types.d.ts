type FigmaNodeResponse = {
  name: string;
  lastModified: string;
  thumbnailUrl: string;
  version: string;
  role: string;
  nodes: FigmaNodes;
};

type FigmaNodes = {
  [nodeId: string]: {
    document: FigmaDocument;
  };
};

type FigmaDocument = {
  id: string;
  name: string;
  type: string;
  blendMode: string;
  children: FigmaDocument[];
  absoluteBoundingBox: {
    x: number;
    y: number;
    width: number;
    height: number;
  };
  background: Background[];
  backgroundColor: {
    r: number;
    g: number;
    b: number;
    a: number;
  };
  cornerRadius: number;
  paddingLeft: number;
  paddingRight: number;
  paddingTop: number;
  paddingBottom: number;
};

type Background = {
  blendMode: string;
  type: string;
  color: any;
};
