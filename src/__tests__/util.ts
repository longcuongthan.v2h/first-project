export const testPaddings = (figmaDocument: FigmaDocument | undefined, styles: CSSStyleDeclaration) => {
  expect(styles.paddingLeft).toBe(`${figmaDocument?.paddingLeft}px`);
  expect(styles.paddingRight).toBe(`${figmaDocument?.paddingRight}px`);
  expect(styles.paddingTop).toBe(`${figmaDocument?.paddingTop}px`);
  expect(styles.paddingBottom).toBe(`${figmaDocument?.paddingBottom}px`);
};
