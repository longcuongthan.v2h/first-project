// eslint-disable-next-line no-unused-vars
import { FunctionComponent, ReactElement } from 'react';
import { render as rtlRender } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { renderSync } from 'node-sass';
import _ from 'lodash';

type RenderOptions = {
  wrapper?: FunctionComponent | undefined;
  stylesheet?: string;
};

function render(ui: ReactElement, options: RenderOptions = {}) {
  const view = rtlRender(ui, {
    ..._.omit(options, 'stylesheet'),
  });

  const styles = renderSync({ file: 'dist/libs/index.css' });

  const styleElement = document.createElement('style');
  styleElement.innerHTML = styles.css.toString();
  document.body.appendChild(styleElement);
  document.body.appendChild(view.container);

  return view;
}

export * from '@testing-library/react';
// override React Testing Library's render with our own
export { render };
