import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Callout, ICalloutProps, Intent } from '@blueprintjs/core';

export default {
  title: 'Component/Callout',
  component: Callout,
  parameters: {
    status: 'Completed',
    docs: {
      description: {
        component:
          'Callouts visually highlight important content for the user. They can contain a title, an icon and content. Each intent has a default icon associated with it.',
      },
    },
  },
  argTypes: {
    intent: {
      control: {
        type: 'select',
        options: Object.values(Intent),
      },
      table: {
        defaultValue: { summary: `"${Intent.NONE}"` },
      },
    },
    icon: {
      description: `Name of a Blueprint UI icon (or an icon element) to render on the left side. If this prop is omitted or undefined, the intent prop will determine a default icon.
       If this prop is explicitly null, no icon will be displayed (regardless of intent).`,
      control: {
        type: 'text',
      },
      table: {
        defaultValue: { summary: `"null"` },
      },
    },
  },
} as Meta;

const Template: Story<ICalloutProps> = (args) => {
  return <Callout {...args} />;
};
export const callout = Template.bind({});
callout.args = {
  title: 'Heading H6',
  children: 'This is an example of a default callout. This is a subtitle text.',
  icon: null,
};
