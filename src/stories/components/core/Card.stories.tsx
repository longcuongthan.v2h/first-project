import React, { useEffect, useState } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Card, ICardProps } from '@blueprintjs/core';

const ListElevation = ['FLAT 01', 'ELEVATION 01', 'ELEVATION 02', 'ELEVATION 03'];

export default {
  title: 'Component/Card',
  component: Card,
  parameters: {
    status: 'Completed',
    docs: {
      description: {
        component: 'A card is a bounded unit of UI content with a solid background color.',
      },
    },
  },
  argTypes: {
    elevation: {
      control: {
        type: 'select',
        options: ListElevation,
      },
      table: {
        defaultValue: { summary: `"FLAT 01"` },
      },
    },
    interactive: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
  },
} as Meta;

interface Props extends ICardProps {
  elevation: any;
}

export interface StylesDictionary {
  [Key: string]: React.CSSProperties;
}

const Template: Story<Props> = (args) => {
  const [elevation, setElevation] = useState<any>(ListElevation.indexOf(args.elevation) || 0);
  useEffect(() => {
    setElevation(ListElevation.indexOf(args.elevation));
  }, [args.elevation]);

  const myStyle: StylesDictionary = {
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    card: {
      minWidth: '220px',
    },
  };

  return (
    <div style={myStyle.container}>
      <Card style={myStyle.card} {...args} elevation={elevation} />
    </div>
  );
};
export const card = Template.bind({});
card.args = {
  elevation: 'FLAT 01',
  interactive: false,
};
