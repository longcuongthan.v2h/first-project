import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Button, ButtonGroup, IButtonGroupProps, Intent } from '@blueprintjs/core';

export default {
  title: 'Component/Button Group',
  component: ButtonGroup,
  parameters: {
    status: 'Completed',
    docs: {
      description: {
        component: 'Button groups arrange multiple buttons in a horizontal or vertical group.',
      },
    },
  },
  argTypes: {
    fill: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    large: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    minimal: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    vertical: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
  },
} as Meta;

const Template: Story<IButtonGroupProps> = (args) => {
  return (
    <ButtonGroup {...args}>
      <Button intent={Intent.PRIMARY} text='Primary' />
      <Button intent={Intent.SUCCESS} text='Success' />
      <Button intent={Intent.WARNING} text='Warning' />
      <Button intent={Intent.DANGER} text='Danger' />
    </ButtonGroup>
  );
};

export const buttonGroup = Template.bind({});
buttonGroup.args = {
  fill: false,
  large: false,
  minimal: false,
  vertical: false,
};
