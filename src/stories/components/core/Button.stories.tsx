import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Alignment, Button, IButtonProps, Intent } from '@blueprintjs/core';

export default {
  title: 'Component/Button',
  component: Button,
  parameters: {
    status: 'Completed',
    docs: {
      description: {
        component: 'Button trigger actions when clicked.',
      },
    },
  },
  argTypes: {
    alignText: {
      control: {
        type: 'select',
        options: Object.values(Alignment),
      },
      table: {
        defaultValue: { summary: '"center"' },
      },
    },
    intent: {
      control: {
        type: 'select',
        options: [Intent.NONE, Intent.PRIMARY],
      },
      table: {
        defaultValue: { summary: `"${Intent.PRIMARY}"` },
      },
    },
    icon: {
      control: {
        type: 'text',
      },
    },
    rightIcon: {
      control: {
        type: 'text',
      },
    },
    text: {
      control: {
        type: 'text',
      },
      table: {
        defaultValue: { summary: '"Button"' },
      },
    },
    active: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    disabled: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    fill: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    loading: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    large: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    small: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    minimal: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
    outlined: {
      control: {
        type: 'boolean',
      },
      table: {
        defaultValue: { summary: false },
      },
    },
  },
} as Meta;

const Template: Story<IButtonProps> = (args) => {
  const newProps = args;
  if (!newProps.icon) newProps.icon = undefined;
  if (!newProps.rightIcon) newProps.rightIcon = undefined;
  return <Button {...newProps} />;
};

export const button = Template.bind({});
button.args = {
  text: 'Button',
  rightIcon: null,
  active: false,
  disabled: false,
  fill: false,
  intent: Intent.PRIMARY,
  loading: false,
  large: false,
  small: false,
  minimal: false,
  outlined: false,
};
