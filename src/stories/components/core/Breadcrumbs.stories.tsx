import React, { Fragment, useState } from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Boundary, Breadcrumbs, Card, IBreadcrumbProps, IBreadcrumbsProps, Slider } from '@blueprintjs/core';

export default {
  title: 'Component/Breadcrumbs',
  component: Breadcrumbs,
  parameters: {
    status: 'Completed',
    docs: {
      description: {
        component: 'Breadcrumbs identify the path to the current resource in an application.',
      },
    },
  },
  argTypes: {
    collapseFrom: {
      control: {
        type: 'select',
        options: Object.values(Boundary),
      },
      table: {
        defaultValue: { summary: `"${Boundary.START}"` },
      },
    },
  },
} as Meta;

const Template: Story<IBreadcrumbsProps> = (args) => {
  const ITEMSDISABLED: IBreadcrumbProps[] = [
    { text: 'Menu 3', href: '#' },
    { text: 'Menu 2', href: '#' },
    { text: 'Menu 1', href: '#' },
    { href: '#', text: 'First Section' },
    { text: 'Second Section', disabled: true },
    { text: 'Third Section', current: true },
  ];
  const [width, setWidth] = useState(50);
  const handleChangeWidth = (width: number) => setWidth(width);
  const renderLabel = (value: number) => {
    return `${value}%`;
  };
  const myStyle = {
    display: 'flex',
    flexWrap: 'wrap',
    padding: '30px 15px',
  } as React.CSSProperties;
  return (
    <Fragment>
      <div style={myStyle}>
        <Slider
          labelRenderer={renderLabel}
          labelStepSize={10}
          min={10}
          max={100}
          onChange={handleChangeWidth}
          showTrackFill={false}
          value={width}
        />
      </div>
      <div style={myStyle}>
        <Card elevation={0} style={{ width: `${width}%` }}>
          <Breadcrumbs
            {...args}
            popoverProps={{
              minimal: true,
              position: 'bottom',
            }}
          />
        </Card>
      </div>
      <div style={myStyle}>
        <Card elevation={0} style={{ width: `${width}%` }}>
          <Breadcrumbs
            {...args}
            items={ITEMSDISABLED}
            popoverProps={{
              minimal: true,
              position: 'bottom',
            }}
          />
        </Card>
      </div>
    </Fragment>
  );
};
const ITEMS: IBreadcrumbProps[] = [
  { text: 'Menu 3', href: '#' },
  { text: 'Menu 2', href: '#' },
  { text: 'Menu 1', href: '#' },
  { href: '#', text: 'First Section' },
  { href: '#', text: 'Second Section' },
  { text: 'Third Section', current: true },
];

export const breadcrumbs = Template.bind({});
breadcrumbs.args = {
  items: ITEMS,
  collapseFrom: Boundary.START,
};
