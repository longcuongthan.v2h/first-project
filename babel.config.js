module.exports = (api) => {
  const isTest = api.env('test')

  // Add preset only for Jest
  return isTest
    ? {
        presets: [
          [
            '@babel/preset-env',
            {
              targets: { node: 'current' }
            }
          ]
        ]
      }
    : {}
}
