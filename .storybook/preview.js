import { addParameters } from '@storybook/react';

addParameters({
  statuses: {
    Completed: '#00B140',
    UAT: '#002C5F',
    'In Progress': '#EA7600'
  },
});

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: { expanded: true, hideNoControlsWarning: true },
  backgrounds: {
    default: 'Default',
    values: [
      {
        name: 'Default',
        value: '#F5F5F5',
      },
      {
        name: 'Light',
        value: '#FFFFFF',
      },
      {
        name: 'Black',
        value: '#000000'
      }
    ]
  }
}
